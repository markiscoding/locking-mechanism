import sys
with open('check_lockdown', 'r') as file:
    text = file.read()
if text == 'deactivated':
    print('Lockdown is already deactivated.')
    sys.exit(0)
else:
    from pyfirmata import ArduinoMega, SERVO, OUTPUT
    from subprocess import run
    from time import sleep 
    import pygame

    pygame.mixer.init()
    port = '/dev/ttyACM0'
    board = ArduinoMega(port)
    sleep(5)

    board.digital[13].mode = SERVO
    board.digital[12].mode = SERVO
    board.digital[10].mode = OUTPUT

    def set_first_servo(angle):
        board.digital[13].write(angle)
        sleep(0.015)
    def set_second_servo(angle):
        board.digital[12].write(angle)
        sleep(0.015)

    a, b, c, d, e = 0, 135, True, True, 135

    while c:
        for i in range(0, b):
            set_first_servo(i)
            i += 1
            if i > 134:
                break
        c = False
    while d:
        for position in range(e, 0, -1):
            set_second_servo(position)
            e -= 1
            if e < 1:
                break
        d = False

    board.digital[10].write(1)
    pygame.mixer.music.load('lockdown_aborted.mp3')
    pygame.mixer.music.play()
    while pygame.mixer.music.get_busy(): 
        pygame.time.Clock().tick(10)
    
    with open('check_lockdown', 'w') as file:
        file.write('deactivated')

    board.exit()
    run('sudo reboot', shell=True)