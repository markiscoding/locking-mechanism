from subprocess import run
import sys

with open('check_door', 'r') as file:
    text = file.read()
if text == 'locked':
    print('Door is already locked.')
    sys.exit(0)
else:
    from pyfirmata import ArduinoMega, SERVO, OUTPUT
    from time import sleep
    import pygame, sensor

    port = '/dev/ttyACM0'
    board = ArduinoMega(port)
    sleep(5)

    board.digital[13].mode = SERVO
    board.digital[12].mode = SERVO
    board.digital[11].mode = OUTPUT
    pygame.mixer.init()
    pygame.mixer.music.load('locking.mp3')

    def set_first_servo(angle):
        board.digital[13].write(angle)
        sleep(0.015)

    def set_second_servo(angle):
        board.digital[12].write(angle)
        sleep(0.015)
    pygame.mixer.music.play()
    while pygame.mixer.music.get_busy():
        pygame.time.Clock().tick(10)

    for position in range(100, 0, -1):
         pass

    for position in range(0, 110):
        set_second_servo(position)

    board.digital[11].write(1)
    with open('check_door', 'w') as file:
        file.write('locked')
    print('Locked.')
    board.exit()
#    run('python3 sensor.py', shell=True)
#    run('python3 tag_detector.py', shell=True)