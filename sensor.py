#! /usr/bin/env python
with open('check_door', 'r') as file:
    door = file.readline()

with open('alert_check', 'r') as file2:
    alert = file2.readline()
if door == 'locked' and alert == 'disabled':

    import RPi.GPIO as GPIO
    import pygame, time, os


    with open('pid', 'w') as file:
        file.write('{}'.format(os.getpid()))

    with open('alert_check', 'w') as file2:
        file2.write('enabled')

    pygame.mixer.init()
    pygame.mixer.music.load('SGC-Alarm.mp3')
    def set_off():
        GPIO.setmode(GPIO.BCM)
        trig = 23
        echo = 24
        GPIO.setup(trig, GPIO.OUT)
        GPIO.setup(echo, GPIO.IN)
        GPIO.output(trig, False)
        time.sleep(1)
        GPIO.output(trig, True)
        time.sleep(0.00001)
        GPIO.output(trig, False)
        while GPIO.input(echo) == 0:
            pulse_start = time.time()
        while GPIO.input(echo) == 1:
            pulse_end = time.time()
        pulse_duration = pulse_end - pulse_start
        distance = pulse_duration * 17150
        distance = round(distance, 2)
#        print(distance)
        if distance < 90:
            while True:
                pygame.mixer.music.play()
                while pygame.mixer.music.get_busy():
                    pygame.time.Clock().tick(20)
    try:
        while True:
            set_off()
    except KeyboardInterrupt:
        GPIO.cleanup()
