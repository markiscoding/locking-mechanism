import sys

def lockdown_init():
    with open('check_lockdown', 'r') as file:
        text = file.read()
    if text == 'activated':
        print('Lockdown is already activated.')
        sys.exit(0)
    else:
        from pyfirmata import ArduinoMega, SERVO, OUTPUT
        from time import sleep
        import pygame

        with open('check_lockdown', 'w') as file:
            file.write('activated')

        port = '/dev/ttyACM0'
        board = ArduinoMega(port)
        sleep(5)

        def set_first_servo(angle):
            board.digital[13].write(angle)
            sleep(0.015)

        def set_second_servo(angle):
            board.digital[12].write(angle)
            sleep(0.015)

        board.digital[13].mode = SERVO
        board.digital[12].mode = SERVO
        board.digital[11].mode = OUTPUT
        pygame.mixer.init()
        pygame.mixer.music.load('lockdown_init.mp3')
        pygame.mixer.music.play()
        while pygame.mixer.music.get_busy():
            pygame.time.Clock().tick(10)
    
        sleep(2)
        pygame.mixer.music.load('lockdown.mp3')
        pygame.mixer.music.play()
        while pygame.mixer.music.get_busy():
            pygame.time.Clock().tick(10)

        for position in range(100, 0, -1):
            pass

        for position in range(0, 100):
            set_second_servo(position)

        board.digital[11].write(1)
        board.exit()

if __name__ == '__main__':
    lockdown_init()