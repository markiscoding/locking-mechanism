import MFRC522, pygame
import RPi.GPIO as GPIO
from subprocess import run
from time import sleep
		
pygame.mixer.init()
		
tag_reader = MFRC522.MFRC522()
print('Waiting for IDC ...')
print('Ctrl-C to stop.')

try:
    while True:
        (status, TagType) = tag_reader.MFRC522_Request(tag_reader.PICC_REQIDL)
        (status, uid) = tag_reader.MFRC522_Anticoll()
        if status == tag_reader.MI_OK:
            if uid[0] == 215:
                with open('check_door', 'r') as file:
                    door = file.readline().rstrip()
                if door == 'locked':
                    print('IDC accepted')
                    pygame.mixer.music.load('idc_accepted.mp3')
                    pygame.mixer.music.play()
                    while pygame.mixer.music.get_busy(): 
                        pygame.time.Clock().tick(5)
                    run('python3 access_for_carlos.py', shell=True)

                elif door == 'unlocked':
                    print('Locking door ....')
                    pygame.mixer.music.load('locking_init.mp3')
                    pygame.mixer.music.play()
                    while pygame.mixer.music.get_busy(): 
                        pygame.time.Clock().tick(5)
                    run('python3 lock.py', shell=True)

            else:
                with open('check_door', 'r') as file:
                    door = file.readline().rstrip()
                if door == 'locked':
                    print('IDC accepted')
                    pygame.mixer.music.load('idc_accepted.mp3')
                    pygame.mixer.music.play()
                    while pygame.mixer.music.get_busy(): 
                        pygame.time.Clock().tick(5)
                    run('python3 unlock.py', shell=True)
                elif door == 'unlocked':
                    print('Locking door ....')
                    pygame.mixer.music.load('locking_init.mp3')
                    pygame.mixer.music.play()
                    while pygame.mixer.music.get_busy(): 
                        pygame.time.Clock().tick(5)
                    run('python3 lock.py', shell=True)
except KeyboardInterrupt:
    GPIO.cleanup()